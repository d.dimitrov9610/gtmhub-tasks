
<img src="https://downloads.intercomcdn.com/i/o/9804/15e5406af965eee8e62a42a6/logo.png" alt="logo" width="300px" style="margin-top: 20px;"/>

### 1. Description
Migrating data from one account to another.
### 2. Get Api token and Account ID (if you don't know how)
1. Log in to `Gtmhub`
2. Navigate to `Setup > Configuration`
3. Select API tokens from the menu on the left
### 3. Setup
1. Open file `constant.js`
2. Add your API key from both of your new and old account.
3. Add your User ID from both of your new and old account.
1. Open file `start.js`.
2. Run `npm install` in terminal to restore all dependencies.
3. write in terminal `node start.js`
4. Enjoy!
