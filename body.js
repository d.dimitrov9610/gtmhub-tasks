const usersBody = (email, firstName, lastName) => {
  return { email, firstName, lastName };
};

const sessionBody = (title, start, end, status, color, parentId) => {
  return { title, start, end, status, parentId, color };
};

const objectiveBody = (
  name,
  dateFrom,
  dateTo,
  attainmentTypeString,
  attainment,
  ownerId,
  sessionId,
  parentGoalSessionId,
  parentId
) => {
  return {
    name,
    dateFrom,
    dateTo,
    attainmentTypeString,
    attainment,
    ownerId,
    sessionId,
    parentGoalSessionId,
    parentId,
  };
};

const metricBody = (
  name,
  goalId,
  ownerId,
  targetOperator,
  dateCreated,
  target,
  initialValue,
  actual,
  attainment,
  critical,
  dynamic,
  metricHistory,
  confidence,
  manualType
) => {
  return {
    name,
    goalId,
    ownerId,
    targetOperator,
    dateCreated,
    target,
    initialValue,
    actual,
    attainment,
    critical,
    dynamic,
    metricHistory,
    confidence,
    manualType,
  };
};

const taskBody = (name, dateCreated, ownerId, parentType, parentId, status) => {
  return {
    name,
    dateCreated,
    ownerId,
    parentType,
    parentId,
    status,
  };
};

export { usersBody, sessionBody, objectiveBody, metricBody, taskBody };
