import {
  request,
  employeesPath,
  sessionsPath,
  goalsPath,
  tasksPath,
  metricsPath,
  usersPath,
} from "./constants.js";
import {
  sessionBody,
  usersBody,
  objectiveBody,
  metricBody,
  taskBody,
} from "./body.js";

const postRequest = async (URL, header, fetchData) => {
  const postUsers = async () => {
    const userData = await Promise.all(
      fetchData.usersData
        .filter((el) => el.email !== "d.dimitrov9610@gmail.com")
        .map(async (el) => {
          const response = await request(
            URL,
            usersPath,
            "POST",
            header,
            usersBody(el.email, el.firstName, el.lastName)
          );
          const data = await response.json();
          return data;
        })
    );
    return userData;
  };

  const usersData = await postUsers();

  const postParentSession = async () => {
    const parentSessions = fetchData.sessionsData.filter(
      (el) => !el.parentId.length
    );
    const sessionsData = await Promise.all(
      parentSessions.map(async (el) => {
        const response = await request(
          URL,
          sessionsPath,
          "POST",
          header,
          sessionBody(el.title, el.start, el.end, el.status, el.color)
        );
        const data = await response.json();
        return data;
      })
    );
    return sessionsData;
  };

  const parentSessionData = await postParentSession();

  const postChildrenSession = async () => {
    const childrenSessions = fetchData.sessionsData.filter(
      (el) => el.parentId.length
    );
    const sessionsData = await Promise.all(
      childrenSessions.map(async (el) => {
        const parentData = parentSessionData.find(
          (e) => e.title === el.parentName
        );
        const response = await request(
          URL,
          sessionsPath,
          "POST",
          header,
          sessionBody(
            el.title,
            el.start,
            el.end,
            el.status,
            el.color,
            parentData.id
          )
        );
        const data = await response.json();
        return data;
      })
    );
    return sessionsData;
  };

  const childrenSessionData = await postChildrenSession();
  const allSessions = [...childrenSessionData, ...parentSessionData];

  const postParentObjectives = async () => {
    const parentObjectives = fetchData.goalsData.filter((el) => !el.parentId);
    const objectivesData = await Promise.all(
      parentObjectives.map(async (el) => {
        const session = allSessions.find((e) => e.title === el.sessionName);
        const ownerUser = usersData.find((e) => e.firstName === el.ownerName);
        const response = await request(
          URL,
          goalsPath,
          "POST",
          header,
          objectiveBody(
            el.name,
            el.dateFrom,
            el.dateTo,
            el.attainmentTypeString,
            el.attainment,
            ownerUser.id,
            session.id
          )
        );
        const data = await response.json();
        return data;
      })
    );
    return objectivesData;
  };

  const parentObjectivesData = await postParentObjectives();

  const postChildrenObjectives = async () => {
    const childrenObjectives = fetchData.goalsData.filter(
      (el) => el.parentId.length
    );
    const objectivesData = await Promise.all(
      childrenObjectives.map(async (el) => {
        const session = allSessions.find((e) => e.title === el.sessionName);
        const ownerUser = usersData.find((e) => e.firstName === el.ownerName);
        const parent = parentObjectivesData.find(
          (e) => e.name === el.parentName
        );
        const parentSession = allSessions.find(
          (e) => e.title === el.parentGoalSessionName
        );
        const response = await request(
          URL,
          goalsPath,
          "POST",
          header,
          objectiveBody(
            el.name,
            el.dateFrom,
            el.dateTo,
            el.attainmentTypeString,
            el.attainment,
            ownerUser.id,
            session.id,
            parentSession.id,
            parent.id
          )
        );
        const data = await response.json();
        return data;
      })
    );
    return objectivesData;
  };

  const childrenObjectivesData = await postChildrenObjectives();
  const allObjectivesData = [
    ...childrenObjectivesData,
    ...parentObjectivesData,
  ];

  const postMetrics = async () => {
    const metricsData = await Promise.all(
      fetchData.metricsData.map(async (el) => {
        const ownerUser = usersData.find((e) => e.firstName === el.ownerName);
        const goal = allObjectivesData.find((e) => e.name === el.goalName);
        const metricHistory = el.metricHistory.map((e) => {
          const userMatch = usersData.find(
            (user) => user.firstName === e.changedByName
          );
          return {
            attainmentChange: e.attainmentChange,
            changedBy: userMatch.id,
            comment: e.comment ? e.comment : " ",
            confidenceValue: e.confidenceValue,
            createdAt: e.createdAt,
            currentAttainment: e.currentAttainment,
            updatedAt: e.updatedAt,
            valueChange: e.valueChange,
          };
        });
        const response = await request(
          URL,
          metricsPath,
          "POST",
          header,
          metricBody(
            el.name,
            goal.id,
            ownerUser.id,
            el.targetOperator,
            el.dateCreated,
            el.target,
            el.initialValue,
            el.actual,
            el.attainment,
            el.critical,
            el.dynamic,
            metricHistory ? metricHistory : null,
            el.confidence,
            el.manualType
          )
        );
        const data = await response.json();
        return data;
      })
    );
    return metricsData;
  };

  const metricsData = await postMetrics();

  const postTasks = async () => {
    const tasksData = await Promise.all(
      fetchData.tasksData.map(async (el) => {
        const owner = usersData.find((e) => e.firstName === el.ownerName);
        if (el.parentType === "metric") {
          const parent = metricsData.find((e) => e.name === el.parentName);
          const response = await request(
            URL,
            tasksPath,
            "POST",
            header,
            taskBody(
              el.name,
              el.dateCreated,
              owner.id,
              el.parentType,
              parent.id,
              el.status
            )
          );
          const data = await response.json();
          return data;
        } else {
          const parent = allObjectivesData.find(
            (e) => e.name === el.parentName
          );
          const response = await request(
            URL,
            tasksPath,
            "POST",
            header,
            taskBody(
              el.name,
              el.dateCreated,
              owner.id,
              el.parentType,
              parent.id,
              el.status
            )
          );
          const data = await response.json();
          return data;
        }
      })
    );
    return tasksData;
  };

  await postTasks();
};

export { postRequest };
