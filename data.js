import {
  headers,
  URL,
  request,
  employeesPath,
  sessionsPath,
  goalsPath,
  tasksPath,
  metricsPath,
} from "./constants.js";

const fetchData = async () => {
  const fetchUsers = async () => {
    const response = await request(URL, employeesPath, "GET", headers);
    const data = await response.json();
    const users = data.items;
    return users;
  };

  const user = await fetchUsers();

  const fetchSessions = async () => {
    const response = await request(URL, sessionsPath, "GET", headers);
    const data = await response.json();
    const sessions = data.items;
    return sessions.map((el) => {
      if (el.parentId) {
        const parent = sessions.find((e) => e.id === el.parentId);
        return {
          id: el.id,
          title: el.title,
          accountId: el.accountId,
          parentId: el.parentId,
          parentName: parent.title,
          start: el.start,
          end: el.end,
          status: el.status,
          color: el.color,
        };
      } else {
        return {
          id: el.id,
          title: el.title,
          accountId: el.accountId,
          parentId: el.parentId,
          start: el.start,
          end: el.end,
          status: el.status,
          color: el.color,
        };
      }
    });
  };
  const sessionData = await fetchSessions();

  const fetchObjectives = async () => {
    const response = await request(URL, goalsPath, "GET", headers);
    const data = await response.json();
    const goals = data.items;
    return goals.map((el) => {
      const owner = user.find((e) => e.id === el.ownerId);
      const session = sessionData.find((e) => e.id === el.sessionId);
      const parentSession = sessionData.find(
        (e) => e.id === el.parentGoalSessionId
      );
      if (el.parentId) {
        const parent = goals.find((e) => e.id === el.parentId);
        return {
          name: el.name,
          id: el.id,
          sessionId: el.sessionId,
          sessionName: session.title,
          ownerId: el.ownerId,
          ownerName: owner.firstName,
          createdById: el.createdById,
          dateFrom: el.dateFrom,
          dateTo: el.dateTo,
          attainmentTypeString: el.attainmentTypeString,
          attainment: el.attainment,
          parentId: el.parentId,
          parentName: parent.name,
          parentGoalSessionName: parentSession.title,
          parentGoalSessionId: el.parentGoalSessionId,
        };
      } else {
        return {
          name: el.name,
          id: el.id,
          sessionId: el.sessionId,
          sessionName: session.title,
          ownerId: el.ownerId,
          ownerName: owner.firstName,
          createdById: el.createdById,
          dateFrom: el.dateFrom,
          dateTo: el.dateTo,
          attainmentTypeString: el.attainmentTypeString,
          attainment: el.attainment,
          parentId: el.parentId,
        };
      }
    });
  };
  const goals = await fetchObjectives();

  const fetchMetrics = async () => {
    const response = await request(URL, metricsPath, "GET", headers);
    const data = await response.json();
    const metrics = data.items;
    return metrics.map((el) => {
      const owner = user.find((e) => e.id === el.ownerId);
      const goal = goals.find((e) => e.id === el.goalId);
      const metricHistory = el.metricHistory.map((history) => {
        const historyUser = user.find((e) => e.id === history.changedBy);
        return {
          id: history.id,
          updatedAt: history.updatedAt,
          createdAt: history.createdAt,
          metricValue: history.metricValue,
          confidenceValue: history.confidenceValue,
          changedByName: historyUser.firstName,
          valueChange: history.valueChange,
          attainmentChange: history.attainmentChange,
          currentAttainment: history.currentAttainment,
          comment: history.comment,
        };
      });
      return {
        name: el.name,
        id: el.id,
        goalId: el.goalId,
        goalName: goal.name,
        ownerId: el.ownerId,
        ownerName: owner.firstName,
        targetOperator: el.targetOperator,
        dateCreated: el.dateCreated,
        target: el.target,
        initialValue: el.initialValue,
        actual: el.actual,
        critical: el.critical,
        attainment: el.attainment,
        progressStatus: el.progressStatus,
        dynamic: el.dynamic,
        metricHistory: metricHistory,
        confidence: el.confidence,
        manualType: el.manualType,
      };
    });
  };
  const metrics = await fetchMetrics();

  const fetchTasks = async () => {
    const response = await request(URL, tasksPath, "GET", headers);
    const data = await response.json();
    const tasks = data.items;
    return tasks.map((el) => {
      const owner = user.find((e) => e.id === el.ownerId);
      if (el.parentType === "metric") {
        const parent = metrics.find((e) => e.id === el.parentId);
        return {
          name: el.name,
          id: el.id,
          ownerId: el.id,
          ownerName: owner.firstName,
          parentId: el.parentId,
          parentName: parent.name,
          status: el.status,
          parentType: el.parentType,
        };
      } else if (el.parentType === "goal") {
        const parent = goals.find((e) => e.id === el.parentId);
        return {
          name: el.name,
          id: el.id,
          ownerId: el.id,
          dateCreated: el.dateCreated,
          ownerName: owner.firstName,
          parentId: el.parentId,
          parentName: parent.name,
          status: el.status,
          parentType: el.parentType,
        };
      }
    });
  };

  const tasks = await fetchTasks();

  return {
    usersData: user,
    sessionsData: sessionData,
    goalsData: goals,
    metricsData: metrics,
    tasksData: tasks,
  };
};

export { fetchData };
