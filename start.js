import { postRequest } from "./request.js";
import { fetchData } from "./data.js";
import { newHeaders, newURL } from "./constants.js";

const migrateData = async () => {
  const oldAccData = await fetchData();
  await postRequest(newURL, newHeaders, oldAccData);
};

migrateData();
