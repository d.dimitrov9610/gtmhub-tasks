import fetch from "node-fetch";

const URL = "https://app.us.gtmhub.com/api/v1";
const newURL = "https://app.gtmhub.com/api/v1";

const employeesPath = "/employees";
const goalsPath = "/goals";
const metricsPath = "/metrics";
const tasksPath = "/tasks";
const sessionsPath = "/sessions";
const usersPath = "/users";

const accountId = ""; // add your old account ID
const apiToken = ""; // add your old account Token
const newAccountId = ""; // add your new account ID
const newApiToken = ""; // add your new account Token

const newHeaders = {
  Authorization: "Bearer " + newApiToken,
  "gtmhub-accountId": newAccountId,
  "Content-Type": "application/json",
};
const headers = {
  Authorization: "Bearer " + apiToken,
  "gtmhub-accountId": accountId,
  "Content-Type": "application/json",
};

const request = async (URL, path, method, header, data) => {
  return await fetch(`${URL}${path}`, {
    method: method,
    headers: header,
    body: JSON.stringify(data),
  });
};

export {
  URL,
  headers,
  newHeaders,
  newURL,
  request,
  employeesPath,
  tasksPath,
  goalsPath,
  metricsPath,
  sessionsPath,
  usersPath,
};
